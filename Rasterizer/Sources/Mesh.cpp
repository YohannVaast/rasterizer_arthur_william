//////////////////////////////////////////////////////////////////////////////
/*	FILE_NAME:	Mesh.cpp						PROJECT: Resterizer			*/
/*																			*/
/*	Author:	Arthur_CARRIERE													*/
/*	Description: Source file for mesh										*/
/*																			*/
/*	Modified_By:	William perez											*/
/*	Because:	CreateSphere, createCube									*/
//////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <stdlib.h>
#include <cmath>

#include "../Includes/Mesh.h"

Mesh* Mesh::CreateTriangle()
{
	Mesh *mesh = new Mesh();

	Vertex vert[3];

	vert[0].Position = {0, 0.5, 0};
	vert[1].Position = {0.5, -0.5, 0};
	vert[2].Position = {-0.5, -0.5, 0};

	for(int i = 0; i < 3; ++i)
	{
		mesh->Vertices.push_back(vert[i]);
	}

	mesh->Indices.push_back(0);
	mesh->Indices.push_back(1);
	mesh->Indices.push_back(2);

	return mesh;

}

Mesh* Mesh::CreateCube()
{
	Mesh * mesh = new Mesh();

	Vertex vert[8];

	vert[0].Position = {-0.5f, -0.5f, -0.5f};
	vert[1].Position = {0.5f, -0.5f, -0.5f};
	vert[2].Position = {0.5f, 0.5f, -0.5f};
	vert[3].Position = {-0.5f, 0.5f, -0.5f};

	vert[4].Position = {-0.5f, -0.5f, 0.5f};
	vert[5].Position = {0.5f, -0.5f, 0.5f};
	vert[6].Position = {0.5f, 0.5f, 0.5f};
	vert[7].Position = {-0.5f, 0.5f, 0.5f};

	for(int i = 0; i < 8; ++i)
	{
		mesh->Vertices.push_back(vert[i]);
	}

	//front
	mesh->Indices.push_back(0);
	mesh->Indices.push_back(1);
	mesh->Indices.push_back(2);

	mesh->Indices.push_back(0);
	mesh->Indices.push_back(2);
	mesh->Indices.push_back(3);
	//end front

	//top
	mesh->Indices.push_back(3);
	mesh->Indices.push_back(2);
	mesh->Indices.push_back(6);

	mesh->Indices.push_back(3);
	mesh->Indices.push_back(6);
	mesh->Indices.push_back(7);
	//end top

	//left
	mesh->Indices.push_back(0);
	mesh->Indices.push_back(3);
	mesh->Indices.push_back(7);

	mesh->Indices.push_back(4);
	mesh->Indices.push_back(0);
	mesh->Indices.push_back(7);
	//end left

	//right
	mesh->Indices.push_back(1);
	mesh->Indices.push_back(5);
	mesh->Indices.push_back(6);

	mesh->Indices.push_back(1);
	mesh->Indices.push_back(6);
	mesh->Indices.push_back(2);
	//end right

	//back
	mesh->Indices.push_back(5);
	mesh->Indices.push_back(4);
	mesh->Indices.push_back(6);

	mesh->Indices.push_back(6);
	mesh->Indices.push_back(4);
	mesh->Indices.push_back(7);
	//end back



	return mesh;
}

Mesh* Mesh::CreateSphere(int LatitudeCont, int LongitudeCount)
{
 Mesh* mesh = new Mesh();

// Longitude |||
int nbLong = LatitudeCont;
// Latitude ---
int nbLat = LongitudeCount;

int length =(nbLong+1) * nbLat + 2;
 
 //vertices
Vertex vertices[length];
float _pi = M_PI;
float _2pi = _pi * 2;

vertices[0].Position.X = 0;
vertices[0].Position.Y = 1;
vertices[0].Position.Z = 0;

for( int lat = 0; lat < nbLat; lat++ )
{
	float a1 = _pi * (float)(lat+1) / (nbLat+1);
	float sin1 = sin(a1);
	float cos1 = cos(a1);
 
	for( int lon = 0; lon <= nbLong; lon++ )
	{
		float a2 = _2pi * (float)(lon == nbLong ? 0 : lon) / nbLong;
		float sin2 = sin(a2);
		float cos2 = cos(a2);
 
        vertices[ lon + lat * (nbLong + 1) + 1].Position.X = (sin1 * cos2);
        vertices[ lon + lat * (nbLong + 1) + 1].Position.Y = cos1;
        vertices[ lon + lat * (nbLong + 1) + 1].Position.Z = (sin1 * sin2);

	}
}

vertices[length-1].Position.X = 0;
vertices[length-1].Position.Y = -1;
vertices[length-1].Position.Z = 0;
//end vertices
 
// Triangles
int nbFaces = length;
int nbTriangles = nbFaces * 2;
int nbIndexes = nbTriangles * 3;
int triangles[nbIndexes];
 
//Top Cap
int i = 0;
for( int lon = 0; lon < nbLong; lon++ )
{
	triangles[i++] = lon+2;
	triangles[i++] = lon+1;
	triangles[i++] = 0;
}
 
//Middle
for( int lat = 0; lat < nbLat - 1; lat++ )
{
	for( int lon = 0; lon < nbLong; lon++ )
	{
		int current = lon + lat * (nbLong + 1) + 1;
		int next = current + nbLong + 1;
 
		triangles[i++] = current;
		triangles[i++] = current + 1;
		triangles[i++] = next + 1;
 
		triangles[i++] = current;
		triangles[i++] = next + 1;
		triangles[i++] = next;
	}
}
 
//Bottom Cap
for( int lon = 0; lon < nbLong; lon++ )
{
	triangles[i++] = length - 1;
	triangles[i++] = length - (lon+2) - 1;
	triangles[i++] = length - (lon+1) - 1;
}
//endregion

for(int i = 0; i < length; ++i)
{
    mesh->Vertices.push_back(vertices[i]);
}

for(int i = 0; i < nbIndexes; ++i)
{
    mesh->Indices.push_back(triangles[i]);
}

return mesh;


 
//mesh.vertices = vertices;

//mesh.triangles = triangles;
}
