//////////////////////////////////////////////////////////////////////////////
/*	FILE_NAME:	Mat4.cpp					PROJECT: Resterizer				*/
/*																			*/
/*	Author:	Arthur_CARRIERE													*/
/*	Description: 															*/
/*																			*/
/*	Modified_By:															*/
/*	Because:																*/
//////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include "../Includes/Mat4.h"
#include "Vec4.h"

Mat4::Mat4()
{
    identity();
}

Mat4 Mat4::CreateTranslationMatrix(const Vec3& vector)
{
    Mat4 result;
    for(unsigned int i = 0; i < 4; ++i)
    {
        for(unsigned int j = 0; j < 4; ++j)
        {
            if(j == 3)
            {
                if(i == 0)
                {
                    result.matrix[i][j] = vector.X;
                }
                else if(i == 1)
                {
                    result.matrix[i][j] = vector.Y;
                }
                else if(i == 2)
                {
                    result.matrix[i][j] = vector.Z;
                }
                else if(i == 3)
                {
                    result.matrix[i][j] = 1;
                }
            }
        }
    }

    return result;
}

Mat4 Mat4::CreateTransformMatrix( const Vec3& rotation, const Vec3& position, const Vec3& scale )
{
    Mat4 result;

    result = CreateTranslationMatrix(position) * CreateYRotationMatrix(rotation.Y) * CreateXRotationMatrix(rotation.X) * CreateZRotationMatrix(rotation.Z) * CreateScaleMatrix(scale);

    return result;

}

Mat4::Mat4(float array[4][4])
{
    for(unsigned int i = 0; i < 4; ++i)
    {
        for(unsigned int j = 0; j < 4; ++j)
        {
            matrix[i][j] = array[i][j];
        }
    }
}

Mat4::Mat4(float array[8])
{
    int counter = 0;
    for(unsigned int i = 0; i < 4; ++i)
    {
        for(unsigned int j = 0; j < 4; ++j, ++counter)
        {
            matrix[i][j] = array[counter];
        }
    }
}

std::ostream& operator<<(std::ostream &os, Mat4 &m)
{
    for(unsigned int i = 0; i < 4; ++i)
    {
        for(unsigned int j = 0; j < 4; ++j)
        {
            os<<m.matrix[i][j]<<"\t";
        }
        os<<std::endl;
    }
    return os;
}

Mat4 Mat4::operator +(Mat4 &m)
{
    Mat4 r(matrix);

    r.add(m);

    return r;
}

void Mat4::zero()
{
    for(unsigned int i = 0; i < 4; ++i)
        for(unsigned int j = 0; j < 4; ++j)
            matrix[i][j] = 0;
}

void Mat4::identity()
{
    for(unsigned int i = 0; i < 4; ++i)
    {
        for(unsigned int j = 0; j < 4; ++j)
        {
            if(i == j)
                matrix[i][j] = 1;
            else
                matrix[i][j] = 0;
        }
    }
}

Mat4 Mat4::operator *(const Mat4 &m)
{
    Mat4 result;

    result.zero();

    for(unsigned int i = 0; i < 4; ++i)
    {
        for(unsigned int j = 0; j < 4; ++j)
        {
            for(unsigned int k = 0; k < 4; ++k)
            {
                result.matrix[i][j] += matrix[i][k] * m.matrix[k][j];
            }
        }
    }

    return result;
}

//Modify after vec4 is finished
Vec4 Mat4::operator *(Vec4 &V)
{
    float temp[] = {V.X, V.Y, V.Z, V.W};

    Vec4 result;

    float tempx = 0;
    float tempy = 0;
    float tempz = 0;
    float tempw = 0;

    for(unsigned int i = 0; i < 4; ++i)
    {
        for(unsigned int j = 0; j < 4; ++j)
        {
            if(i == 0)
            {
                tempx += matrix[i][j] * temp[j];
            }
            else if(i == 1)
            {
                tempy += matrix[i][j] * temp[j];
            }
            else if(i == 2)
            {
                tempz += matrix[i][j] * temp[j];
            }
            else if(i == 3)
            {
                tempw += matrix[i][j] * temp[j];
            }
        }
    }

    result.X = tempx;
    result.Y = tempy;
    result.Z = tempz;
    result.W = tempw;

    return result;
}

bool Mat4::operator ==(Mat4 &m)
{
    for(unsigned int i = 0; i < 4; ++i)
    {
        for(unsigned int j = 0; j < 4; ++j)
            if(matrix[i][j] != m.matrix[i][j])
                return false;
    }

    return true;
}

Mat4 Mat4::CreateXRotationMatrix(float angle)
{
    Mat4 result;

    result.matrix[1][1] = cos(angle);
    result.matrix[1][2] = -sin(angle);
    result.matrix[2][1] = sin(angle);
    result.matrix[2][2] = cos(angle);

    return result;
}

Mat4 Mat4::CreateYRotationMatrix(float angle)
{
    Mat4 result;

    result.matrix[0][0] = cos(angle);
    result.matrix[0][2] = sin(angle);
    result.matrix[2][0] = -sin(angle);
    result.matrix[2][2] = cos(angle);

    return result;
}

Mat4 Mat4::CreateZRotationMatrix(float angle)
{
    Mat4 result;

    result.matrix[0][0] = cos(angle);
    result.matrix[0][1] = -sin(angle);
    result.matrix[1][0] = sin(angle);
    result.matrix[1][1] = cos(angle);

    return result;
}

Mat4 Mat4::CreateScaleMatrix(const Vec3& value)
{
    Mat4 result;

    result.matrix[0][0] = value.X;
    result.matrix[1][1] = value.Y;
    result.matrix[2][2] = value.Z;
    result.matrix[3][3] = 1;


    return result;
}

void Mat4::add(Mat4 &m)
{
    for(unsigned int i = 0; i < 4; ++i)
    {
        for(unsigned int j = 0; j < 4; ++j)
        {
            matrix[i][j] += m.matrix[i][j];
        }
    }
}

void Mat4::multiply(Mat4 &m)
{
    Mat4 result;

    for(unsigned int i = 0; i < 4; ++i)
        for(unsigned int j = 0; j < 4; ++j)
            result.matrix[i][j] = 0;

    for(unsigned int i = 0; i < 4; ++i)
    {
        for(unsigned int j = 0; j < 4; ++j)
        {
            for(unsigned int k = 0; k < 4; ++k)
            {
                result.matrix[i][j] += matrix[i][k] * m.matrix[k][j];
            }
        }
    }

    *this = result;
}

bool Mat4::isIdentity()
{
    Mat4 ref;

    for(unsigned int i = 0; i < 4; ++i)
    {
        for(unsigned int j = 0; j < 4; ++j)
        {
            if(ref.matrix[i][j] != matrix[i][j])
                return false;
        }
    }
    return true;
}

bool isIdentity(Mat4 &m)
{
    Mat4 ref;

    for(unsigned int i = 0; i < 4; ++i)
    {
        for(unsigned int j = 0; j < 4; ++j)
        {
            if(ref.matrix[i][j] != m.matrix[i][j])
                return false;
        }
    }
    return true;
}

bool Mat4::isOrthogonal()
{
    float array[4];
    float result_dot = 0;

    float result = 1;
    float add = 0;

    for(unsigned int i = 0; i < 3; ++i)
    {
        add = 0;
        for(unsigned int j = 0; j < 3; ++j)
        {
            result = matrix[j][i];
            result *= result;
            add += result;

            array[j] = matrix[j][i];
        }

        if(sqrt(add) != 1)
            return false;
        else
        {

            for(unsigned int j = i + 1; j < 3; ++j)
            {
                for(unsigned int k = 0; k < 3; ++k)
                {
                    result_dot += array[k] * matrix[k][j];
                }
                if(0 != result_dot)
                {
                    return false;
                }
            }
        }
    }


    return true;
}

bool Mat4::computeInverse(Mat4 *matrixArray, unsigned int numberMatrices, Mat4 &res)
{
    Mat4 tempArray[numberMatrices];
    for(unsigned int i = 0; i < numberMatrices; ++i)
    {
        if(matrixArray[i].isOrthogonal())
        {
            tempArray[numberMatrices - i - 1] = matrixArray[i].transpose();
        }
        else
        {
            return false;
        }
    }

    for(unsigned int j = 0; j < numberMatrices; ++j)
        res = matrixArray[j] * tempArray[j];

    return true;

}

bool Mat4::computeInverse(Mat4 matrix, Mat4 &res)
{
    Mat4 temp;

    if(matrix.isOrthogonal())
    {
        temp = matrix.transpose();
    }

    res = matrix * temp;

    return true;

}

Mat4 Mat4::transpose()
{
    Mat4 result;

    for(unsigned int i = 0; i < 4; ++i)
    {
        for(unsigned int j = 0; j < 4; ++j)
        {
            result.matrix[i][j] = matrix[j][i];
        }
    }

    return result;
}

Mat4::~Mat4()
{
}
