//////////////////////////////////////////////////////////////////////////////
/*	FILE_NAME: Vec4.cpp								PROJECT: Resterizer		*/
/*																			*/
/*	Author:	Arthur_CARRIERE													*/
/*	Description: Source file for homogenized 3D Vectors						*/
/*																			*/
/*	Modified_By:															*/
/*	Because:																*/
//////////////////////////////////////////////////////////////////////////////
#include "Vec3.h"
#include "Vec4.h"
#include <cmath>
 
										// Constructor
Vec4::Vec4(const Vec3& v, float w)
{
	X = v.X;
	Y = v.Y;
	Z = v.Z;
	W = w;
}
										// Public Methods
void	Vec4::Homogenize()
{
	if (W != 0)
	{
		X /= W;
		Y /= W;
		Z /= W;
	}
	else return;
}

float 	Vec4::GetMagnitude() const
{
	return sqrt(X*X + Y*Y + Z*Z);
}

void	Vec4::Normalize()
{
	float	length = GetMagnitude();
	X /= length;
	Y /= length;
	Z /= length;
}

Vec4	Vec4::Add(Vec4 v)
{
	X += v.X;
	Y += v.Y;
	Z += v.Z;
	return *this;
}

Vec4	Vec4::Scale(float f)
{
	X *= f;
	Y *= f;
	Z *= f;
	return *this;
}

										// Operators overloads
Vec4& 	Vec4::operator+(Vec4 v)
{
	Add(v);
	return *this;
}

Vec4& 	Vec4::operator*(float f)
{
	Scale(f);
	return *this;
}
