#include <iostream>

#include "../Includes/Texture.h"

Texture::Texture(unsigned int _width, unsigned int _height) : width(_width), height(_height)
{
    pixels = new Color[width * height];

    for(unsigned int i = 0; i < width * height; ++i)
    {
        pixels[i].R = 0;
        pixels[i].G = 0;
        pixels[i].B = 0;
        pixels[i].A = 255;
    }

}

void Texture::SetPixelColor(unsigned int x, unsigned int y, const Color &c)
{
    pixels[y * width + x] = c;
}

Texture::~Texture()
{
    delete pixels;
}
