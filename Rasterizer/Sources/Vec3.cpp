//////////////////////////////////////////////////////////////////////////////
/*	FILE_NAME: Vec3.cpp								PROJECT: Resterizer		*/
/*																			*/
/*	Author:	Arthur_CARRIERE													*/
/*	Description: Source file for 3D Vectors									*/
/*																			*/
/*	Modified_By:															*/
/*	Because:																*/
//////////////////////////////////////////////////////////////////////////////
#include "Vec3.h"
#include <cmath>
										// Constructor
Vec3::Vec3(float x, float y, float z)
{
	X = x;
	Y = y;
	Z = z;
}
										// Public Methods
float 	Vec3::GetMagnitude() const
{
	return sqrt(X*X + Y*Y + Z*Z);
}

void	Vec3::Normalize()
{
	float	length = GetMagnitude();
	X /= length;
	Y /= length;
	Z /= length;
}

Vec3	Vec3::Add(Vec3 v)
{
	X += v.X;
	Y += v.Y;
	Z += v.Z;
	return *this;
}

Vec3	Vec3::Scale(float f)
{
	X *= f;
	Y *= f;
	Z *= f;
	return *this;
}

										// Operators overloads
Vec3& 	Vec3::operator+(Vec3 v)
{
	Add(v);
	return *this;
}

Vec3& 	Vec3::operator*(float f)
{
	Scale(f);
	return *this;
}
