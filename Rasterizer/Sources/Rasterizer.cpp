/////////////////////////////////////////////////////////////////////////////////
/*	FILE_NAME:                          PROJECT: Resterizer                    */
/*                                                                             */
/*	Author:           WillKillu                                                */
/*	Description:                                                               */
/*                                                                             */
/*	Modified_By:                                                               */
/*	Because:                                                                   */
/////////////////////////////////////////////////////////////////////////////////

#include <iostream>

#include <SDL2/SDL.h>

#include "../Includes/Rasterizer.h"

Rasterizer::Rasterizer()
{
	SDL_Init(SDL_INIT_VIDEO);

	if(window == 0)
	{
		printf("Error creating Window\n");
		SDL_Quit();

	}

	window = SDL_CreateWindow("OpenGL", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 1024, 768, SDL_WINDOW_BORDERLESS);

	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

}

Rasterizer::~Rasterizer()
{

}

void Rasterizer::RenderScene(Scene* pScene, Texture* pTarget )
{
	(void) pScene;
	(void) pTarget;


}

