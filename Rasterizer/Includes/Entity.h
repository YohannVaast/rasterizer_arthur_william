//////////////////////////////////////////////////////////////////////////////
/*	FILE_NAME:	Entity.h						PROJECT: Resterizer			*/
/*																			*/
/*	Author:	Arthur_CARRIERE													*/
/*	Description: Header file for entity class								*/
/*																			*/
/*	Modified_By:															*/
/*	Because:																*/
//////////////////////////////////////////////////////////////////////////////
#pragma once

#include "Mesh.h"
#include "Mat4.h"

class Entity
{

public:
    //constructor(s)/destructor
    Entity();

    ~Entity();

    //static variables

    //public variables
	
    //public methods

    //operator overload


private:
    //private variables
	Mesh* 	TheMesh;
	Mat4	Transformation;
    //private methods


};

//os operator overload

