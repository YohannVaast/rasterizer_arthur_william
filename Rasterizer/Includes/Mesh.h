//////////////////////////////////////////////////////////////////////////////
/*	FILE_NAME:	Mesh.h							PROJECT: Resterizer			*/
/*																			*/
/*	Author:	Arthur_CARRIERE													*/
/*	Description: Header file for mesh										*/
/*																			*/
/*	Modified_By:															*/
/*	Because:																*/
//////////////////////////////////////////////////////////////////////////////
#pragma once
#include <stdlib.h>
#include <vector>

#include "Vertex.h"

class Mesh
{

public:
    //constructor(s)/destructor
    Mesh() = default;

    virtual ~Mesh() = default;

    //static variables

    //public variables
    std::vector<Vertex>	Vertices;
	std::vector<int> 	Indices;
	
    //public methods
    static Mesh* CreateTriangle();
	static Mesh* CreateCube();
	static Mesh* CreateSphere(int LatitudeCount, int LongitudeCount);
	
    //operator overload


private:
    //private variables

    //private methods


};

//os operator overload

