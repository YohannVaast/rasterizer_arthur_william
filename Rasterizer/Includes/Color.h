//////////////////////////////////////////////////////////////////////////////
/*	FILE_NAME:	Color.h						PROJECT: Resterizer				*/
/*																			*/
/*	Author:	Arthur_CARRIERE													*/
/*	Description: Header file for colors										*/
/*																			*/
/*	Modified_By:															*/
/*	Because:																*/
//////////////////////////////////////////////////////////////////////////////
#pragma once

class Color
{

public:
    //constructor(s)/destructor
    Color() = default;
    virtual ~Color() = default;

    //static variables

    //public variables
	unsigned char R;
	unsigned char G;
	unsigned char B;
	unsigned char A;
	
    //public methods
	
    //operator overload


private:
    //private variables

    //private methods


};

//os operator overload

