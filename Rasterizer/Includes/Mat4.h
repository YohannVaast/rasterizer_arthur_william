/////////////////////////////////////////////////////////////////////////////////
/*	FILE_NAME: Matrix4                  		PROJECT: Resterizer            */
/*                                                                             */
/*	Author: William Perez                                                      */
/*	Description: Squared matrix of 4 by 4                                      */
/*                                                                             */
/*	Modified_By:                                                               */
/*	Because:                                                                   */
/////////////////////////////////////////////////////////////////////////////////
#pragma once

#include <cmath>
#include "Vec3.h"

class Vec4;

class Mat4
{
    //static variables

    //public variables
public:
    float matrix[4][4];

    //constructor(s)
    Mat4();

    Mat4( float array[4][4]);

    Mat4( float array[8]);

    static Mat4 CreateTranslationMatrix( const Vec3& vector);

    static Mat4 CreateTransformMatrix( const Vec3& rotation, const Vec3& position, const Vec3& scale );

    static Mat4 CreateScaleMatrix(const Vec3& value);

    static Mat4 CreateXRotationMatrix(float angle);

    static Mat4 CreateYRotationMatrix(float angle);

    static Mat4 CreateZRotationMatrix(float angle);

    //public methods

    void zero();
    void identity();

    void add(Mat4 &m);
    void multiply(Mat4 &m);

    Mat4 transpose();

    //destructor
    virtual ~Mat4();

    //operator overload
    Mat4 operator+(Mat4 &m);
    Mat4 operator*(const Mat4 &m);
    bool operator==(Mat4 &m);
    Vec4 operator*(Vec4 &V);

    bool isIdentity();
    bool isOrthogonal();

    bool computeInverse(Mat4 *matrixArray, unsigned int numberMatrices, Mat4 &res);

    bool computeInverse(Mat4 matrix, Mat4 &res);

    // Matrix operator*(T x, T y);

    //private variables
    private:

    //private methods

};

    //os operator overload
    std::ostream& operator<<(std::ostream &os, Mat4 &m);
