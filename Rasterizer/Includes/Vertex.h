//////////////////////////////////////////////////////////////////////////////
/*	FILE_NAME:	Vertex.h						PROJECT: Resterizer			*/
/*																			*/
/*	Author:	Arthur_CARRIERE													*/
/*	Description: Header file for Vertex		 								*/
/*																			*/
/*	Modified_By:															*/
/*	Because:																*/
//////////////////////////////////////////////////////////////////////////////
#pragma once

#include "Vec3.h"

class Vertex
{

public:
    //constructor(s)/destructor
    Vertex() = default;

    virtual ~Vertex() = default;

    //static variables

    //public variables
	Vec3 	Position;
    //public methods

    //operator overload


private:
    //private variables

    //private methods


};

//os operator overload

