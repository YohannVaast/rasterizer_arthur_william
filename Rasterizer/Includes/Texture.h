#pragma once
#include "Color.h"

class Texture
{

public:
    //static variables

    //public variables

    //constructor(s)
    Texture() = delete;
    Texture(unsigned int _width, unsigned int _height);

    //destructor
    ~Texture();

    //public methods
    void SetPixelColor(unsigned int x, unsigned int y, const Color& c);


    //operator overload

private:

    //private variables
    unsigned int width;
    unsigned int height;

    Color* pixels;

    //private methods

};

//os operator overload
