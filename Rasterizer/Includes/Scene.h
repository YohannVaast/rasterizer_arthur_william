//////////////////////////////////////////////////////////////////////////////
/*	FILE_NAME:	Scene.h							PROJECT: Resterizer			*/
/*																			*/
/*	Author:	Arthur_CARRIERE													*/
/*	Description: Header file for scene class								*/
/*																			*/
/*	Modified_By:															*/
/*	Because:																*/
//////////////////////////////////////////////////////////////////////////////
#pragma once
#include "Entity.h"

class Scene
{

public:
    //constructor(s)/destructor
    Scene();

    ~Scene();

    //static variables

    //public variables

    //public methods

    //operator overload


private:
    //private variables
	std::vector<Entity>	Entities;
	
    //private methods


};

//os operator overload

