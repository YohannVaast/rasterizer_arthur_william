//////////////////////////////////////////////////////////////////////////////
/*	FILE_NAME:	Vec4.cpp						PROJECT: Resterizer			*/
/*																			*/
/*	Author:	Arthur_CARRIERE													*/
/*	Description: Header file for homogenized 3D	Vectors						*/
/*																			*/
/*	Modified_By:															*/
/*	Because:																*/
//////////////////////////////////////////////////////////////////////////////
#pragma once
#include "Vec3.h"
 
class Vec4
{

public:
    //constructor(s)/destructor
    Vec4() = default;
	Vec4(const Vec3& v, float w = 1.0f);
    virtual ~Vec4() = default;

    //static variables
    
    //public variables
	float X;
	float Y;
	float Z;
	float W;

    //public methods
    void	Homogenize();
	float	GetMagnitude() const;
	void	Normalize();
	Vec4	Add(Vec4 v);
	Vec4	Scale(float f);
	
	//operator overload
	Vec4& operator+(Vec4 v);
	Vec4& operator*(float f);

private:
    //private variables
	
    //private methods


};

//os operator overload

