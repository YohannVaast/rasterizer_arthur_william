//////////////////////////////////////////////////////////////////////////////
/*	FILE_NAME: Vec3.h							PROJECT: Resterizer			*/
/*																			*/
/*	Author:	Arthur_CARRIERE													*/
/*	Description: Header file for 3D Vectors									*/
/*																			*/
/*	Modified_By:															*/
/*	Because:																*/
//////////////////////////////////////////////////////////////////////////////
#pragma once

class Vec3
{

public:
    //constructor(s)/destructor
    Vec3() = default;
	Vec3(float x, float y, float z);
    virtual ~Vec3() = default;

    //static variables
    
    //public variables
	float X;
	float Y;
	float Z;

    //public methods
	float	GetMagnitude() const;
	void	Normalize();
	Vec3	Add(Vec3 v);
	Vec3	Scale(float f);
	
	//operator overload
	Vec3& operator+(Vec3 v);
	Vec3& operator*(float f);

private:
    //private variables
	
    //private methods


};

//os operator overload

