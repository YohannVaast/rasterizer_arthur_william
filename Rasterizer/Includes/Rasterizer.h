/////////////////////////////////////////////////////////////////////////////////
/*	FILE_NAME:                          PROJECT: Resterizer                    */
/*                                                                             */
/*	Author:         WillKillU                                                  */
/*	Description:                                                               */
/*                                                                             */
/*	Modified_By:                                                               */
/*	Because:                                                                   */
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <SDL2/SDL.h>

#include "Scene.h"
#include "Texture.h"

class Rasterizer
{

public:
    //static variables


    //public variables

    SDL_Window *window;
    SDL_Renderer *renderer;


    //constructor(s)
    Rasterizer();

    //destructor
    ~Rasterizer();

    //public methods
    void RenderScene( Scene* pScene, Texture* pTarget );


    //operator overload

    //private variables
private:

    //private methods

};

//os operator overload
